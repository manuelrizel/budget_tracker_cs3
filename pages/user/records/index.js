import { useState, Fragment, useEffect } from 'react'
import { Card, Button, Row, Col, InputGroup, FormControl, Form, Container } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import moment from 'moment';
import { FaPlusCircle } from "react-icons/fa";
import { GrTransaction } from "react-icons/gr";

export default () => {
    const [searchKeyword, setSearchKeyword] = useState('')
    const [searchType, setSearchType] = useState("All")

    useEffect(()=>{
        setSearchType(searchType);

        setSearchKeyword(searchKeyword);

    },[searchType, searchKeyword])

    return (
        <View title="Records">

        <div className="col-md-12">
            <div className="row">
                <div className="col-md-6 cat-title">
                    <h3><GrTransaction/> Records </h3> 
                </div>
                <div className="col-md-6 cat-button"> 
                <Link href="/user/records/new"><a className="btn btn-success mt-1 mb-3"><FaPlusCircle size={15} className="icon-style" /> Add New Record </a></Link>
                </div>
            </div>
            </div>

            <InputGroup>
                <FormControl placeholder="Search Record" value={ searchKeyword } onChange={ (e) => setSearchKeyword(e.target.value) }/>
                <Form.Control as="select" defaultValue={ searchType } onChange={ (e) => setSearchType(e.target.value) }>
                    <option value="All">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </InputGroup>
            <RecordsView searchKeyword={ searchKeyword } searchType={ searchType }/>
        </View>
    )
}

const RecordsView = ({searchKeyword, searchType}) => {
    const [records, setRecords] = useState([])
    const [searchResults, setSearchResults] = useState([])
    //console.log(searchType)
    useEffect(() => {

        const payload = {
            headers: {
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }
        }

        fetch(`${AppHelper.API_URL}/users/get-records`, payload).then((response) => response.json()).then(records => {
                let viewRecord;

                viewRecord = records.map((record) => {
                    const textColor = (record.type === 'Income') ? 'text-success' : 'text-danger'
                    const cardBorder = (record.type === 'Income') ? 'success' : 'danger'
                    const amountSymbol = (record.type === 'Income') ? '+' : '-'

                if(searchKeyword === ''){
                    if(record.type === searchType){
                      return (         
                        <Card border={cardBorder} className="searchCard" key={ record._id }>
                                <Card.Body>
                                       <Row>
                                   <Col xs={ 6 }>
                                        <h5>{ record.description }</h5>
                                        <h6><span className={ textColor }>{ record.type }</span> { ' (' + record.categoryName + ')' }</h6>
                                        <p>{ moment(record.dateAdded).format("MMMM D, YYYY") }</p>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6 className={ textColor }>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                                        <span className={ textColor }>{ record.balanceAfterTransaction.toLocaleString() }</span>
                                    </Col>
                                </Row> 
                                </Card.Body>
                        </Card>              
                    )
                    }
                    if(searchType === 'All'){
                    return (         
                        <Card border={cardBorder} className="searchCard " key={ record._id }>
                                <Card.Body >
                                       <Row>
                                   <Col xs={ 6 }>
                                        <h5>{ record.description }</h5>
                                        <h6><span className={ textColor }>{ record.type }</span> { ' (' + record.categoryName + ')' }</h6>
                                        <p>{ moment(record.dateAdded).format("MMMM D, YYYY") }</p>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6 className={ textColor }>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                                        <span className={ textColor }>{ record.balanceAfterTransaction.toLocaleString() }</span>
                                    </Col>
                                </Row> 
                                </Card.Body>
                        </Card>              
                    )
                }

                }  else{
                        if(record.description.toLowerCase().startsWith(searchKeyword)){
                        return (         
                            <Card border={cardBorder} className="searchCard" key={ record._id }>
                                    <Card.Body >
                                           <Row>
                                       <Col xs={ 6 }>
                                            <h5>{ record.description }</h5>
                                            <h6><span className={ textColor }>{ record.type }</span> { ' (' + record.categoryName + ')' }</h6>
                                            <p>{ moment(record.dateAdded).format("MMMM D, YYYY") }</p>
                                        </Col>
                                        <Col xs={ 6 } className="text-right">
                                            <h6 className={ textColor }>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                                            <span className={ textColor }>{ record.balanceAfterTransaction.toLocaleString() }</span>
                                        </Col>
                                    </Row> 
                                    </Card.Body>
                            </Card>              
                            )
                        }
                }                  
                })

                setRecords(viewRecord)
            }) 
    },[searchType,searchKeyword])

    return (
        <Fragment>
                {records} 
        </Fragment>       
    )
}