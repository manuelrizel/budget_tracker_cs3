import { useState, useEffect } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import View from '../../../components/View'
import Swal from 'sweetalert2'
import AppHelper from '../../../app-helper';
import { BsCardChecklist } from "react-icons/bs";

export default () => {
    return (
        <View title="New Record">
            <Row className="justify-content-center">
                <Col xs md="6">
                     <Card>
                        <Card.Header><BsCardChecklist size={25}/> Create New Record</Card.Header>
                        <Card.Body>
                            <NewRecordForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewRecordForm = () => {
    const [categoryName, setCategoryName] = useState(undefined)
    const [typeName, setTypeName] = useState(undefined)
    const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState('')
    const [categories, setCategories] = useState([])


    useEffect(()=> {


        const payload = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                }, 
                body: JSON.stringify({
                    name: name
                })
            }

        fetch(`${AppHelper.API_URL}/users/get-categories`, payload).then((response) => response.json()).then(categories => {

            let showCategory
                showCategory = categories.map((category) => {

                    if(category.type === typeName){
                        return (
                            <option key={ category._id } value={ category.name }>{ category.name }</option>
                        )
                    }
                })
                setCategories(showCategory)

            })

    },[typeName])

function addTransactions(e){
        e.preventDefault();
            //check if email exist in our records
            const options = {
                method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                    }, 
                    body: JSON.stringify({
                        categoryName: categoryName,
                        typeName: typeName,
                        amount: amount,
                        description: description
                    })
            }
            fetch(`${AppHelper.API_URL}/users/add-record`, options).then((res) => res.json()).then((data => {

                if(data === true) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Successfully Added'
                                })
                            Router.push('/user/records')
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed.',
                                    text: 'Something went wrong.'
                                })
                            }
            }))
}

    return (
        <Form onSubmit={ e => addTransactions(e)}>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={ typeName } onChange={ (e) => setTypeName(e.target.value) } required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control as="select" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required>
                    <option value selected disabled>Select Category Name</option>
                    {categories}
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control type="number" placeholder="Enter amount" value={ amount } onChange={ (e) => setAmount(parseFloat(e.target.value)) } required/>
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control type="text" placeholder="Enter description" value={ description } onChange={ (e) => setDescription(e.target.value) } required/>
            </Form.Group>
            <Button variant="primary" type="submit" block>Submit</Button>
        </Form>
    )
}

