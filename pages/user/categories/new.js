import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import { VscNewFile } from "react-icons/vsc";




export default () => {
    return (
        <View title="New Category">
            <Row className="justify-content-center mt-5">
                <Col xs md="6">
                    <Card border="secondary">
                        <Card.Header><VscNewFile size={25}/> Create New Category</Card.Header>
                        <Card.Body>
                            <NewCategoryForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewCategoryForm = () => {
    const [categoryName, setCategoryName] = useState('')
    const [typeName, setTypeName] = useState(undefined)

        function createCategory(e){
            e.preventDefault();
            //check if email exist in our records


            const payload = {
                method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                    }, 
                    body: JSON.stringify({
                        name: categoryName,
                        typeName: typeName
                    })
            }
            fetch(`${AppHelper.API_URL}/users/add-category`, payload).then((res) => res.json()).then((data => {

                if(data === true) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Successfully Added',
                                    text: 'Category was added successfully.'
                                })
                            Router.push('/user/categories')
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed.',
                                    text: 'Something went wrong.'
                                })
                            }
            }))
        }


    return (
        <Form onSubmit={ e => createCategory(e)}>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter category name" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={ typeName } onChange={ (e) => setTypeName(e.target.value) } required>
                  <option value selected disabled>Select Category Type</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Button variant="primary" type="submit" block>Submit</Button>
        </Form>
    )
}
