
import { useState, useContext } from 'react'
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import View from '../components/View';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Link from 'next/link'
import UserContext from '../contexts/UserContext';
import { AiOutlineLogin } from "react-icons/ai";
import AppHelper from '../app-helper'
import { GoogleLogin } from 'react-google-login';


export default function Login(){

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { user, setUser } = useContext(UserContext)

  if(user.email !== null){
    Router.push('/user/records');
  }

  const retrieveUserDetails = (accessToken) => {
    //we have to make sure the user has already been verified meaning it was already granted access token.
     const options = {
         headers: { Authorization: `Bearer ${accessToken}`}
     }

    //create a request going to the desired endpoint with the payload.
    fetch(`${AppHelper.API_URL}/users/details`, options).then((response) => response.json()).then(data => {

        setUser({ email: data.email })
        Router.push('/user/records')
    })


  }

  function login(e){
    e.preventDefault();
    fetch(`${AppHelper.API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      }) 
    }).then(res => {
      return res.json()
    }).then(data => {
      if(typeof data.accessToken !== "undefined") {
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        Swal.fire({
          icon: 'success',
          title: 'Successfully Logged In'
        })
      
      } else {
           Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
      }
    })
  }

  const retrieveGoogleDetails = (response) => {
    //console.log(response)

    const payload = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({ tokenId: response.tokenId,
            firstName: response.profileObj.given_name,
            lastName: response.profileObj.family_name,
            email: response.profileObj.email

       })
    }

    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
    .then(res => res.json())
    .then(data => {
      localStorage.setItem('token', data.accessToken)
    const options = {
         headers: { Authorization: `Bearer ${data.accessToken}`}
     }

    //create a request going to the desired endpoint with the payload.
    fetch(`${AppHelper.API_URL}/users/details`, options).then((response) => response.json()).then(data => {

        setUser({ email: data.email })
        Router.push('/user/records')
    })
      

    })
  }



  return (

        <View title={ 'ThriftyWallet - Login' }>
        <Row className="justify-content-center mt-4">
          <Col xs md="6">
              <Card border="secondary">
                 <Card.Header><AiOutlineLogin size={25} /> Log In</Card.Header>
                 <Card.Body>
                     <Form onSubmit={e => login(e)}>        
                      <Form.Group controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          type="email" 
                          placeholder="Enter email"
                          value={email} onChange={e=>setEmail(e.target.value)} required
                        />
                      </Form.Group>
                      <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                          type="password" 
                          placeholder="Password"
                          value={password} onChange={e=>setPassword(e.target.value)} required />
                      </Form.Group>    
                      <Button variant="primary" type="submit" block>
                        Submit
                      </Button>                       
                        <hr />
                      <GoogleLogin
                        clientId="644861028615-f175q9b2ucqd61pi3oq4fpj3mfgigd50.apps.googleusercontent.com"
                        buttonText="Sign in with Google"
                        onSuccess={retrieveGoogleDetails}
                        onFailure={retrieveGoogleDetails}
                        cookiePolicy={'single_host_origin'}
                        theme="dark"
                        className="google-btn" />
                        <br />
                        <br />
                      <p className="text-center">Not Yet Registered?  &nbsp;
                        <Link href="/register"><a>Sign up here!</a></Link>
                      </p>
                      

                    </Form>          
                </Card.Body>
            </Card>
          </Col>
        </Row>
      </View>

    
    )
}