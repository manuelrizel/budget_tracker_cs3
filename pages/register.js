import { useState, useEffect, useContext } from 'react'
import UserContext from '../contexts/UserContext';
import Router from 'next/router'
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import View from '../components/View';
import { RiUserAddLine } from "react-icons/ri";
import AppHelper from '../app-helper'
import { GoogleLogin } from 'react-google-login';

//import Swal
import Swal from 'sweetalert2'


export default function Register(){

	const { user, setUser } = useContext(UserContext);
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState(0);
	const [password1,setPassword1] = useState("");
	const [password2,setPassword2] = useState("");

	const [isActive,setIsActive] = useState(true)

	//the change will run after every user input.
	useEffect(() =>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [firstName, lastName, email, mobileNo, password1, password2])

	//lets now create our register method
	function registerUser(e){
		e.preventDefault();
		//check if email exist in our records
		fetch(`${AppHelper.API_URL}/users/email-exists`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email 
				})//convert to string data type to be accepted in the API

		}).then(res => res.json()).then(data => {
			//create a checker just to see if we can get data
			console.log(data)
			//lets create a control structure to give the appropriate response according to the value of the data.
			if(data === false){
				//if the return is false then allow the user register otherwise nope
				fetch(`${AppHelper.API_URL}/users/register`, {
					method: 'POST',
					headers: {
					'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				}).then(res => res.json()).then(data => {
					console.log(data)
					if(data === true) {
						Swal.fire({
							icon: 'success',
							title: 'Successfully Registered',
							text: 'Thank you for registering.'
						})
						//after displaying a success message, redirect user to the login page
						Router.push('/login')
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Registration Failed.',
							text: 'Something went wrong.'
						})
					}
				})
			}else {
				//the else branch will run if the return value is true
				Swal.fire({
					icon: 'error',
					title: 'Registration Failed',
					text: 'Email is already taken.'
				})
			}
		})
	}

const retrieveGoogleDetails = (response) => {
    console.log(response)

    const payload = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({ tokenId: response.tokenId,
            firstName: response.profileObj.given_name,
            lastName: response.profileObj.family_name,
            email: response.profileObj.email
       })
    }
    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
    .then(res => res.json())
    .then(data => {
      localStorage.setItem('token', data.accessToken)
    const options = {
         headers: { Authorization: `Bearer ${data.accessToken}`}
     }

    //create a request going to the desired endpoint with the payload.
    fetch(`${AppHelper.API_URL}/users/details`, options).then((response) => response.json()).then(data => {
        setUser({ email: data.email })
        Router.push('/user/records')
    })
      
    })
  }

	return (
	<div>
		    <View title={ 'ThriftyWallet - Register' }>
			<Row className="justify-content-center mt-4">
          	<Col xs md="6">
              <Card border="secondary">
                 <Card.Header><RiUserAddLine size={25}/>Register</Card.Header>
                 <Card.Body>
						<Form className="login-form" onSubmit={e => registerUser(e)}>
						<Form.Group controlId="userFirstName">
						    <Form.Label>First Name</Form.Label>
						    <Form.Control 
						    	type="text" 
						    	placeholder="Enter First Name"
						    	value={firstName}
						    	onChange={e => setFirstName(e.target.value)} 
						    	required
						    />
						   
						  </Form.Group>
							<Form.Group controlId="userLastName">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control 
						    	type="text" 
						    	placeholder="Enter Last Name"
						    	value={lastName} 
						    	onChange={e => setLastName(e.target.value)} 
						    	required
						    />
						  </Form.Group>

						  <Form.Group controlId="userEmail">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    	type="email" 
						    	placeholder="Enter email"
						    	value={email} 
						    	onChange={e => setEmail(e.target.value)} 
						    	required
						    />
						  </Form.Group>
						  <Form.Group controlId="mobileNo">
						    <Form.Label>Mobile Number</Form.Label>
						    <Form.Control 
						    	type="number" 
						    	placeholder="Enter Mobile Number"
						    	alue={mobileNo} 
						    	onChange={e => setMobileNo(e.target.value)} 
						    	required
						    />
						  </Form.Group>

						  <Form.Group controlId="password1">
						    <Form.Label>Password</Form.Label>
						    <Form.Control 
						    	type="password" 
						    	placeholder="Password"
						    	value={password1} 
						    	onChange={e => setPassword1(e.target.value)} 
						    	required
						    />
						  </Form.Group>	

						  <Form.Group controlId="password2">
						    <Form.Label>Confirm Password</Form.Label>
						    <Form.Control 
						    	type="password" 
						    	placeholder="Confirm Password"
						    	value={password2} 
						    	onChange={e => setPassword2(e.target.value)} 
						    	required
						    />
						  </Form.Group>		
						  {

						  	isActive
						  	? 
						  <Button variant="primary" type="submit" block>
						    Submit
						  </Button> 
						  : 
						  <Button variant="primary" type="submit" disabled block>
						    Submit
						  </Button>
						  } 

						  <hr />
                      	<GoogleLogin
                        clientId="644861028615-f175q9b2ucqd61pi3oq4fpj3mfgigd50.apps.googleusercontent.com"
                        buttonText="Sign up with Google"
                        onSuccess={retrieveGoogleDetails}
                        onFailure={retrieveGoogleDetails}
                        cookiePolicy={'single_host_origin'}
                        theme="dark"
                        className="google-btn" />
						</Form>
                </Card.Body>
            </Card>
          </Col>
        </Row>
			</View>
		</div>	
	)
}


