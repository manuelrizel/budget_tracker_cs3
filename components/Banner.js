import { Button, Row, Col } from 'react-bootstrap';
import { CgArrowLongRight } from "react-icons/cg";
import Link from 'next/link';



export default function Banner(){
	return (

	<div className="row banner-section">
		<div className="col-md-6 mt-5">
  		<h1>Manage your money well, buddy!</h1>
		  	<p>
		    	Experience a unique way of tracking your expenses easily -- all for free.
		  	</p>
            <Link href="#features"><a className="btn btn-success mt-1 mb-3">Learn More</a></Link>
		  	
		  	</div>
		 <div className="col-md-6 d-none d-lg-block banner-col-2 mt-1"></div>
	</div>


	)
}