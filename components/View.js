import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Head from 'next/head';

const View = ({title, children}) => {
	return (
		<Fragment>
			<Head>
				<title key="title-tag"> {title} </title>
				<link rel="icon" href="/tw.png" />
				<meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Container className="pt-4 mt-5">
				{children}
			</Container>
		</Fragment>
		)
}

export default View;